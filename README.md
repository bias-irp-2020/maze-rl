# Proyecto 2: Aprendizaje reforzado

Instituto Tecnológico de Costa Rica

Escuela de Ingeniería en Electrónica

Introducción al Reconocimiento de Patrones


### Elaborado por:

* Osvaldo Jesús Alfaro González 2016122997
* Alejandro Flores Herrera 2016093687
* Jean Paul López Mejía 2015121022

### Dependencias
NumPy >= 1.18.5

Matplotlib >= 3.2.2

Pytorch >= 1.6.0

### Utilización

En la carpeta "src" se encuentran los archivos "main1.py" y "dqn.py"
los cuales contienen la primera y segunda parte del proyecto,
respectivamente.

Para correr los archivos solo es necesario modificar (si se desea) los
valores relacionados al numero de episodios y tasa de exploración, al
inicio de cada archivo.

Para ejecutar el algoritmo de iteración de valor debe ejecutar:

```bash
python src/main1.py
```

Para ejecutar el algoritmo de DQN debe ejecutar:

```bash
python src/dqn.py
```

en este mismo archivo puede modificar la variable
```python
DEBUG = False
```
Cuando se encuentra en False se ejecuta el programa con una cantidad mínima
de mensajes en consola y no se actualizan los sensores del agente, esto con 
el fin de poder entrenar el agente lo más rápido posible. Si la variable se
setea a True entonces se podrán observar cambios en el agente y se mostrará
información de las acciones que está realizando el agente.

Al final de la ejecución se crearán dos imagenes con el nombre de errors.png
y steps.png. Ellas contendrán las gráficas de error y precisión promedio por
episodio y los pasos por episodio respectivamente. 