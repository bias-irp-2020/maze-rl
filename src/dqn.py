"""dqn.py: Use a maze and solve it with DQN algorithm.
"""
import os 
import env
import time
import math
import torch
import pickle
import random
import dispatcher
import numpy as np
import torch.nn as nn
from collections import deque
import matplotlib.pyplot as plt
import torch.nn.functional as F
from torch.optim import Adam, RMSprop


DEBUG = True #Displays more data for debugging

SAVE_DATA = True
LOAD_DATA = False

## Variables ##
MAX_STEPS = 1000  #max steps per episode
MAX_EPISODES = 100  #max episodes per execution
WEIGHTS_CHANGE_STEPS = 50 #number of steps to change neural net weights

#Epsilon
EXPLORATION_INIT  = 1
EXPLORATION_DECAY = 0.9999
EXPLORATION_MIN   = 0.01

MEMORY_SIZE = 1000000 #has to be large, that's why it is DEEP
BATCH_SIZE = 10
GAMMA = 0.975

#Neural Net
LEARNING_RATE = 1e-3

class Network(nn.Module):

    def __init__(self, observation_space, action_space):
        """ Creates instance of the neural network_q used to predict action that gives the best reward

        # Arguments:
            observation_space: number of components of the state
            action_space: number of actions that can be executed by the agent
        """
        super(Network, self).__init__()
        self.fc1 = nn.Linear(observation_space, 32)
        self.fc2 = nn.Linear(32, 64)
        self.fc3 = nn.Linear(64, 64)
        self.fc4 = nn.Linear(64, 64)
        self.fc5 = nn.Linear(64, action_space)

        self.optimizer = torch.optim.Adam(self.parameters(), lr=LEARNING_RATE)
        #self.optimizer = torch.optim.RMSprop(self.parameters(), lr=LEARNING_RATE)

        #self.loss_func = nn.MSELoss()
        self.loss_func = nn.SmoothL1Loss()

    def forward(self, x):
        ''' Tesorflow predict equivalent

        #Arguments: 
            x: state, np array containing x position, 
                y position, angle and sensor values of the agent

        #Returns:
            Array with the weights of the actions and their respective rewards
        '''
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = F.relu(self.fc4(x))
        return self.fc5(x)

class DQNStepper:
    """Use DQN algorithm to train the model"""

    def __init__(self, dispatch):
        self.dispatch = dispatch
        self.reset()
        self.init()
        self.lastRenderingTime = time.perf_counter()
        
    def reset(self):
        """Account for posible serialization of new envirnoments"""

        print("ExampleStepper.reset() called")

        # Do this to get a valid reference to the environment in use
        self.env = self.dispatch.env()

        # Here an arbitrary step size for the "advance" action
        self.step_size = self.env.maze.cellSizeX/3

        # 20 degrees allows for changes to happen in the sensed enviorement 
        self.actions = [lambda: self.env.agent.advance(self.step_size),
                        lambda: self.env.agent.left(20),
                        lambda: self.env.agent.right(20)]

        self.episode_counter = 0
        self.step_counter = 0

        self.exploration_rate = EXPLORATION_INIT

        self.memory = deque(maxlen=MEMORY_SIZE)
        if LOAD_DATA:self.__load_repository()

        #used to keep track of steps and episodes for plotting
        self.episode_history  = []
        self.step_history     = []
        self.epsilon_history  = []
        self.loss_history     = []
        self.accuracy_history = []
        self.reward_history   = []

        #reset variables 
        # 3 fixed observations (x, y, angle) + number of sensors of the agent
        state_size = (3 + len(self.env.agent.observations)) # x, y, angle + number of sensors
        self.action_space = len(self.actions)      
        self.network_q = Network(state_size * 2, self.action_space)
        self.network_q_m = Network(state_size * 2, self.action_space)
        if LOAD_DATA: self.__load_network_weights()
        self.network_q_m.load_state_dict(self.network_q.state_dict()) #making shure both nets have the same weights

        print(self.network_q)
        
    def init(self):
        """Reset and setup the stepper"""

        # Tell the environment to place the agent at the beginning
        self.env.reset()
        # Ensure the agent's position is properly displayed
        self.dispatch.render()
        self.lastRenderingTime = time.perf_counter()

    def step(self, iteration):
        """Perform one DQN step iteration

        We use step_counter instead of iteration to avoid initialization
        on the first loop       
        """

        #If step counter is zero we are on a new episode
        if(self.step_counter == 0):

            #Initialize state
            # the state is an array containing 
            # agent x postion, y position, angle and observations 
            apx = self.env.agent.state.posX
            apy = self.env.agent.state.posY
            aa = self.env.agent.state.angle
            self.state = torch.tensor([apx,apy,aa] + self.env.agent.observations + [apx,apy,aa] + self.env.agent.observations)

            self.episode_loss_sum = 0
            self.episode_loss_counter = 0
            self.correct_approximation = 0
            self.reward_sum = 0

        action = self.__take_action(self.state)
        #make agent execute action dicted by the network_q
        self.env.tryAction(self.env.agent, self.actions[action]) 

        apx = self.env.agent.state.posX
        apy = self.env.agent.state.posY
        aa = self.env.agent.state.angle
        state_next = torch.cat((torch.tensor([apx,apy,aa] + self.env.agent.observations), self.state[0: 3 + len(self.env.agent.observations)] ))
        done = self.__check_if_done()
        reward = self.__calc_reward(self.state, action, state_next, done)

        self.reward_sum += reward
        
        self.__add_to_memory(self.state, action, reward, state_next, done)

        # Only at most 15fps:
        # - Print information about curr position 
        # - Draw the agent's position and (if activated) the sensors
        if (DEBUG and (time.perf_counter() - self.lastRenderingTime) > 1/15):
            #ugly code, pretty prints
            print(f"[Episode: {self.episode_counter}, Step: {self.step_counter}] Memory size: {len(self.memory)}")
            print(f" State:      x:{ ('{:.2f}'.format(self.state[0].item())).rjust(6) }, y:{ ('{:.2f}'.format(self.state[1].item())).rjust(6) }, { ('{:.1f}'.format(self.state[2].item())).rjust(5)}°, l:{('{:.2f}'.format(self.state[3].item())).rjust(6)}, f:{('{:.2f}'.format(self.state[4].item())).rjust(6)}, r:{('{:.2f}'.format(self.state[5].item())).rjust(6)}")
            print(f" Next State: x:{ ('{:.2f}'.format(state_next[0].item())).rjust(6) }, y:{ ('{:.2f}'.format(state_next[1].item())).rjust(6) }, { ('{:.1f}'.format(state_next[2].item())).rjust(5)}°, l:{('{:.2f}'.format(state_next[3].item())).rjust(6)}, f:{('{:.2f}'.format(state_next[4].item())).rjust(6)}, r:{('{:.2f}'.format(state_next[5].item())).rjust(6)}")
            print(f" epsilon: {'{:.2f}'.format(self.exploration_rate) } {'explore' if self.explore else 'exploit'} -> action: {action}, reward: {'{:.4f}'.format(reward)} + ({'{:.4f}'.format(self.reward_sum) })") 
            print("")
            self.dispatch.render()
            self.lastRenderingTime = time.perf_counter()

        self.state = state_next

        self.__experience_replay()
        
        # Check if the agent as reached the finish cell or max number of steps
        self.step_counter += 1
        if done or self.step_counter >= MAX_STEPS:
            
            loss = self.episode_loss_sum/self.episode_loss_counter
            accuracy = 100 * (self.correct_approximation / (self.episode_loss_counter * BATCH_SIZE))

            self.episode_history.append(self.episode_counter)
            self.step_history.append(self.step_counter)
            self.epsilon_history.append(self.exploration_rate)
            self.loss_history.append(loss)
            self.accuracy_history.append( accuracy)
            self.reward_history.append(self.reward_sum)

            self.episode_counter += 1
                
            print(f"Episode: {self.episode_counter}/{MAX_EPISODES} Steps: {self.step_counter}/{MAX_STEPS} Epsilon: {'{:.2f}'.format(self.exploration_rate)} Reward: {'{:.4f}'.format(self.reward_sum)} Accuracy: {'{:.2f}'.format(accuracy)}% Error: {loss} ")

            #if max episodes not exeded start a new one 
            #otherwise finish the simulation
            if(self.episode_counter < MAX_EPISODES):
                self.step_counter = 0
                self.dispatch.restart()    
            else:
                self.dispatch.stop()
                self.__plot_steps()
                self.__plot_error()
                self.__plot_reward()
                if SAVE_DATA: 
                    self.__save_network_weights()
                    self.__save_repository() 
                self.reset()

    def __check_if_done(self):
        """ Checks if the agents has reached the end cell 

            Returns:
                True if the center of the agent is inside the
                final cell, otherwise returns False
        """

        apx = self.env.agent.state.posX
        apy = self.env.agent.state.posY
        cell_size_x = self.env.maze.cellSizeX
        cell_size_y = self.env.maze.cellSizeY
        end_cell_x = self.env.maze.endX * cell_size_x
        end_cell_y = self.env.maze.endY * cell_size_y

        return (end_cell_x <= apx <= (end_cell_x + cell_size_x) and 
                end_cell_y <= apy <= (end_cell_y + cell_size_y))

    def __take_action(self, state):
        """Returns index of action to be taken

        Based on the epsilon returns a random action or 
        one predicted by the neural network_q. 
        """

        if np.random.rand() < self.exploration_rate:
            self.explore = True
            return random.randrange(self.action_space)
        self.explore = False
        q_values = self.network_q(state)
        return torch.argmax(q_values)

    def __add_to_memory(self, state, action, reward, next_state, done):
        '''Saves data to memory, if memory is full deletes the oldest entry 
        '''
        if(len(self.memory) > MEMORY_SIZE): 
            self.memory.popleft()
        self.memory.append((state, action, reward, next_state, done))

    def __experience_replay(self):
        if len(self.memory) < BATCH_SIZE:
            return
        else:

            minibatch = random.sample(self.memory, BATCH_SIZE)
            minibatch_loss = 0

            for state, action, reward, state_next, done in minibatch:
                y = reward
                if not done:

                    y = (reward + GAMMA * torch.max(self.network_q_m(state_next)))

                Q = self.network_q(state)
                Q_target = torch.empty_like(Q).copy_(Q)
                Q_target[action] = y

                ## train net ##
                self.correct_approximation += (torch.max(Q_target) == torch.max(Q)).sum().item()

                minibatch_loss += self.network_q.loss_func(Q, Q_target) # Compute loss.
            
            minibatch_loss /= BATCH_SIZE

            self.episode_loss_sum = minibatch_loss.item()

            self.network_q.optimizer.zero_grad() # object to zero all of the gradients
            # compute gradient of the loss with respect to model parameters
            minibatch_loss.backward()
            # Calling the step function on an Optimizer makes an update to its parameters
            self.network_q.optimizer.step()

            self.episode_loss_counter += 1     
            self.exploration_rate *= EXPLORATION_DECAY
            self.exploration_rate = max(EXPLORATION_MIN, self.exploration_rate)

            # Every WEIGHTS_CHANGE_STEPS asign network_q weights to network_q_m 
            if(self.step_counter % WEIGHTS_CHANGE_STEPS == 0):
                self.network_q_m.load_state_dict(self.network_q.state_dict())
                 
    def __calc_reward(self, state, action, state_next, done):
        """Returns reward asociated to the given state
        
        Arguments:
            state: original state
            action: index corresponding to the self.actions array
            state_next: state achieved by taking the action

        Returns:
            reward: numeric value that represents the reward of the transition 
        """
        past_state_x = state[6].item()
        past_state_y = state[7].item()
        curr_state_l = state[3].item()
        curr_state_f = state[4].item()
        curr_state_r = state[5].item()
        next_state_x = state[0].item()
        next_state_y = state[1].item()
        next_state_l = state_next[3].item()
        next_state_f = state_next[4].item()
        next_state_r = state_next[5].item()

        if done:
            return 100

        '''
        if( (abs(past_state_x - next_state_x) <= self.step_size/5) and 
            (abs(past_state_y - next_state_y) <= self.step_size/5)):
            return -0.1


        # In case a rotation was executed
        if action > 0:

            # Case: all sensors detect a wall
            #  example: robot is facing a corner or in a dead end 
            # Reward If: The rotation is done to the side where the
            # side sensor detects the greatest distance
            if (0 < curr_state_l  and 0 < curr_state_f and 0 < curr_state_r and
                (
                    curr_state_l > curr_state_r and action == 1 or
                    curr_state_l < curr_state_r and action == 2 
                )):
                return -0.04

            # Case: front sensor and only one of the side sensors
            #       detects a wall
            #  example: robot is facing a wall at a 45 degree angle or 
            #           parallel to a corner
            # Reward If: The rotation is done to the side where the
            # side sensor detects nothing
            if (0 < curr_state_f and 
                ( (0 > curr_state_l) and not (0 > curr_state_r) and action == 1 ) or
                ( (0 > curr_state_r) and not (0 > curr_state_l) and action == 2 )):
                return -0.02

        #if a move forward was executed
        else:

            # Case: Side sensors detects walls on present and next states
            #  example: robot is paralel to a corridor
            # Reward If: The front sensor detects wall, and decrese reward if 
            #if (0 < curr_state_l and 0 < next_state_l and 
            #0 < curr_state_r and 0 < next_state_r):
            if ( 0 < curr_state_l and 0 < curr_state_r ):

                # when front sensor detects nothing on the next state
                if( next_state_f < 0 ):
                    return -0.01

                # when there is a reduction in the front sensor
                if(  curr_state_f > next_state_f):
                    return ( -0.01 * (self.env.agent.maxDistance - next_state_f)) - 0.01

            # Case: Side sensors detects walls on present and next states
            #  example: robot is paralel to a corridor
            # Reward If: The front sensor detects wall, and decrese reward if 
            if( (0 < curr_state_f) and (0 < curr_state_l != 0 < curr_state_r) ):
                return -0.01
        

        return -0.05   # general punishment, if none of the previously stated conditions are met

        '''

        # promedio
        if ( curr_state_f < 0 and next_state_f < 0): 
            return -0.03

        if curr_state_f < 0:
            curr_state_f = self.env.agent.maxDistance

        if next_state_f < 0:
            next_state_f = self.env.agent.maxDistance

        if(curr_state_f < next_state_f):
            return -0.1/(next_state_f - curr_state_f)

        multiplier = 1

        if( (abs(past_state_x - next_state_x) <= self.step_size/5) and 
            (abs(past_state_y - next_state_y) <= self.step_size/5)):
            multiplier = 10

        if(curr_state_f >= next_state_f):    
            return (-0.01 * (curr_state_f - next_state_f)) - 0.01 * multiplier

    def __plot_steps(self):
        fig, ax1 = plt.subplots()

        ax1.set_xlabel('Episode')
        ax1.set_ylim([0,MAX_STEPS + 5]) #leave head room for max range
        ax1.set_ylabel('Steps', color = 'tab:blue')
        ax1.plot(self.episode_history, self.step_history, color = 'tab:blue')

        ax2 = ax1.twinx()

        ax2.set_ylabel('Epsilon', color = 'tab:orange')
        ax2.set_ylim([0,EXPLORATION_INIT + 0.01]) #leave head room for max range
        ax2.plot(self.episode_history, self.epsilon_history, color = 'tab:orange')
        ax2.tick_params(axis='y')

        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        plt.title('Steps and epsilon per episode')
        plt.savefig('graphs/steps.png')  

        print("-Saved Steps and epsilon per episode graph at graphs/steps.png")    

    def __plot_error(self):
        fig, ax1 = plt.subplots()

        ax1.set_xlabel('Episode')
        ax1.set_ylabel('Error', color = 'tab:cyan')
        ax1.plot(self.episode_history, self.loss_history, color = 'tab:cyan')

        ax2 = ax1.twinx()

        ax2.set_ylabel('Accuracy', color = 'tab:olive')
        ax2.set_ylim([0,101]) #leave head room for max range
        ax2.plot(self.episode_history, self.accuracy_history, color = 'tab:olive')
        ax2.tick_params(axis='y')

        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        plt.title('Loss and accuracy per episode')
        plt.savefig('graphs/errors.png')  

        print("-Saved Loss and accuracy per episode graph at graphs/error.png") 

    def __plot_reward(self):
        fig, ax1 = plt.subplots()

        ax1.set_xlabel('Episode')
        ax1.set_ylabel('Reward')
        ax1.plot(self.episode_history, self.reward_history, color = 'tab:green')

        plt.title('Reward per episode')
        plt.savefig('graphs/reward.png')  

        print("-Saved reward per episode graph at graphs/reward.png") 

    def __save_network_weights(self):
        """Stores neural net weights"""
        torch.save(self.network_q.state_dict(), 'data/weights.pt')
        print("-Saved neural net weights")

    def __load_network_weights(self):
        """Stores neural net weights and experience repository in file """
        filename = 'data/weights.pt'
        if os.path.exists(filename):
            self.network_q.load_state_dict(torch.load(filename))
            print("-Loaded neural net weights")
        else:
            print("-Unable to load neural net weights,file 'data/weights.pt' not found.")

    def __save_repository(self):
        """Stores repository dequeue in a file"""
        file = open("data/repository.pickle", 'wb')
        pickle.dump(self.memory, file)
        print("-Saved repository")

    def __load_repository(self):
        """Loads repository dequeue from file"""
        filename = 'data/repository.pickle'
        if os.path.exists(filename):
            self.memory = pickle.load(open(filename, 'rb'))
            print("-Loaded repository")
        else:
            print("-Unable to load repository,file 'data/repository.pickle' not found.")


# #######################
# # Main control center #
# #######################

# This object centralizes everything
theDispatcher = dispatcher.Dispatcher()

# Provide a new environment (maze + agent)
theDispatcher.setEnvironment(env.Environment(12, 8, 15))

# Provide also the simulation stepper, which needs access to the
# agent and maze in the dispatcher.
theDispatcher.setStepper(DQNStepper(theDispatcher))

# Start the GUI and run it until quit is selected
# (Remember Ctrl+\ forces python to quit, in case it is necessary)
theDispatcher.run()
