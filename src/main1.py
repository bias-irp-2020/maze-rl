
"""example.py: Use a maze and solve it with classic wall-following
   algorithm.

   The RL versions should be faster
"""

__author__ = "Pablo Alvarado"
__copyright__ = "Copyright 2020, Pablo Alvarado"
__license__ = "BSD 3-Clause License (Revised)"

import time
import math
import random
import dispatcher
import env

import numpy as np
from os import path
import pickle
import copy as cp
from datetime import datetime
import sys
import matplotlib.pyplot as plt

seed = int(time.time())
np.random.seed(seed)

# Set values
max_episodes = 150

initial_exploration_rate = 1
min_exploration_rate = 0.1
exploration_decay = 0.985

origin_reward = -2
end_reward = 100

gamma = 0.9


# Stepper for project Part 1
class Stepper1:
    """Simple stepper performing a random walk"""

    def __init__(self, dispatch):
        self.dispatch = dispatch
        self.reset()
        self.init()
        self.lastRenderingTime = time.perf_counter()
    
    def saveCounters(self,counters):
    
        file = open('counters.txt', 'wb')
        pickle.dump(counters, file)
                    
        file.close()
        print("Counters saved")
    
    def savePolicyAndV(self,policyAndV):
        file = open('policyAndV.txt', 'wb')
        pickle.dump(policyAndV, file)                      
        file.close()
        print("Policy and V saved")
    
    def loadCounters(self):
        
        if(path.exists("counters.txt") == 1):
            
            file = open('counters.txt', 'rb')      
            counters = pickle.load(file)
            
            self.numCounterA, self.numCounterR, self.numCounterL,\
            self.denCounterA, self.denCounterR, self.denCounterL = counters
            file.close()
            print("File found, counters loaded")
        else:
            print("File not found, counters initialized")
            
    def loadPolicyAndV(self):
        
        if(path.exists("policyAndV.txt") == 1):
            file = open('policyAndV.txt', 'rb')      
            policyAndV = pickle.load(file)
            self.policy, self.V = policyAndV
            file.close()
            print("File found, policy and V loaded")
        else:
            print("File not found, policy and V initialized")
    
    def saveAll(self):
        
        # Saving counters
        counters = [self.numCounterA,self.numCounterR,self.numCounterL,
                    self.denCounterA,self.denCounterR,self.denCounterL]
        print("Stepper1.saveCounters() called")
        self.saveCounters(counters)
        
        # Saving policy and value function
        print("Stepper1.savePolicyAndV() called")
        policyAndV = [self.policy,self.V]
        self.savePolicyAndV(policyAndV)
        
        # Saving policy for debugging
        policyE = np.transpose(np.reshape( self.policy[0,0              :self.numCells  ],(self.xCells,self.yCells) ))
        policyS = np.transpose(np.reshape( self.policy[0,self.numCells  :self.numCells*2],(self.xCells,self.yCells) ))
        policyW = np.transpose(np.reshape( self.policy[0,self.numCells*2:self.numCells*3],(self.xCells,self.yCells) ))
        policyN = np.transpose(np.reshape( self.policy[0,self.numCells*3:self.numCells*4],(self.xCells,self.yCells) ))
        
        np.savetxt('policyE.csv',policyE.astype(int),delimiter=',')
        np.savetxt('policyS.csv',policyS.astype(int),delimiter=',')
        np.savetxt('policyW.csv',policyW.astype(int),delimiter=',')
        np.savetxt('policyN.csv',policyN.astype(int),delimiter=',')
        
        # Saving value function for debugging
        valueE = np.transpose(np.reshape( self.V[0              :self.numCells  ,0],(self.xCells,self.yCells) ))
        valueS = np.transpose(np.reshape( self.V[self.numCells  :self.numCells*2,0],(self.xCells,self.yCells) ))
        valueW = np.transpose(np.reshape( self.V[self.numCells*2:self.numCells*3,0],(self.xCells,self.yCells) ))
        valueN = np.transpose(np.reshape( self.V[self.numCells*3:self.numCells*4,0],(self.xCells,self.yCells) ))

        np.savetxt('valueE.csv',valueE,delimiter=',')
        np.savetxt('valueS.csv',valueS,delimiter=',')
        np.savetxt('valueW.csv',valueW,delimiter=',')
        np.savetxt('valueN.csv',valueN,delimiter=',')
        
    def reset(self):
        """Account for posible serialization of new envirnoments"""
        print("Stepper1.reset() called")

        # Do this to get a valid reference to the environment in use
        self.env = self.dispatch.env()

        # You can for instance get the number of cells in the maze with:
        self.xCells = self.env.maze.nx
        self.yCells = self.env.maze.ny

        # Or also get the size of a cell
        self.cellSizeX = self.env.maze.cellSizeX
        self.cellSizeY = self.env.maze.cellSizeY

        # Here an arbitrary step size for the "advance" action
        step = self.env.maze.cellSizeX

        # Possible actions
        self.actions = [lambda: self.env.agent.advance(step),
                        lambda: self.env.agent.right(90),
                        lambda: self.env.agent.left(90)
                        ]
        
        # Number of cells
        self.numCells = self.env.maze.nx * self.env.maze.ny
        
        # Number of states
        self.numStates = self.numCells * 4
        
        # Counters for Probabilities
        self.numCounterA = np.ones((self.numStates,self.numStates))
        self.numCounterR = np.ones((self.numStates,self.numStates))
        self.numCounterL = np.ones((self.numStates,self.numStates))
            
        self.denCounterA = np.ones((self.numStates,1))*self.numStates
        self.denCounterR = np.ones((self.numStates,1))*self.numStates
        self.denCounterL = np.ones((self.numStates,1))*self.numStates
        
        print("Stepper1.loadCounters() called")
        self.loadCounters()
        
        self.info = []
        self.total_time = datetime.now()
        self.start = datetime.now()
        self.end = datetime.now()
        self.steps = 0

   
        # Rewards
        # Distance from start to end
        finX, finY = self.env.maze.endX, self.env.maze.endY
        
        minReward = -0.25

        self.RewardsA = np.ones((self.numStates,self.numStates))*(minReward)
        self.RewardsR = cp.copy(self.RewardsA) - 0.75
        self.RewardsL = cp.copy(self.RewardsA) - 0.75
        
        # Diagonal items represent that the agent advanced but it hit a wall       
        for i in range(self.numStates):
            self.RewardsA[i,i] = -0.7     
        
        endIndexE = ((0*self.numCells) + (finX)*self.yCells
                     + finY)
        endIndexS = ((1*self.numCells) + (finX)*self.yCells
                     + finY)
        endIndexW = ((2*self.numCells) + (finX)*self.yCells
                     + finY)
        endIndexN = ((3*self.numCells) + (finX)*self.yCells
                     + finY)
        
        for i in range(self.numStates):
            self.RewardsA[i,endIndexE] = end_reward
            self.RewardsA[i,endIndexS] = end_reward
            self.RewardsA[i,endIndexW] = end_reward
            self.RewardsA[i,endIndexN] = end_reward
            
            self.RewardsA[i,0*self.numCells] = origin_reward
            self.RewardsA[i,1*self.numCells] = origin_reward
            self.RewardsA[i,2*self.numCells] = origin_reward
            self.RewardsA[i,3*self.numCells] = origin_reward        
        
        self.Rewards = np.array([self.RewardsA,self.RewardsR,self.RewardsL])
# =============================================================================
#         # Distance from start to end
#         finX, finY = self.env.maze.endX, self.env.maze.endY
#         distance = np.sqrt(finX**2 + finY**2)
#         
#         minReward = -0.25
#         maxReward = -0.01
#         
#         numRadius = 4
#         
#         # Diffent reward radius size
#         radius = distance / numRadius
#         
#         rewardValues = np.linspace(minReward,maxReward,numRadius)
#         
#         self.RewardsA = np.ones((self.numStates,self.numStates))
#         
#         for i in range(np.size(self.RewardsA,0)):
#             for j in range(np.size(self.RewardsA,1)):
# 
#                 # calculating distance from cell to start
#                 pos = np.mod(j,self.numCells);
#                 x = np.floor(pos/self.yCells);
#                 y = np.mod(pos,self.yCells);
#                 cellDist = np.sqrt(x**2 + y**2)
#                 
#                 index = int(np.floor(cellDist/radius))
#                 
#                 if index != numRadius:
#                     self.RewardsA[i,j] = rewardValues[index]
#                 else:
#                     # Reward for reaching the ending cell
#                     self.RewardsA[i,j] = 100
# 
#         self.RewardsR = cp.copy(self.RewardsA)
#         self.RewardsL = cp.copy(self.RewardsA)
#         
#         # Diagonal items represent that the agent advanced but it hit a wall
#         for i in range(self.numStates):
#             self.RewardsA[i,i] = -0.5
#         
#         self.Rewards = np.array([self.RewardsA,self.RewardsR,self.RewardsL])
# =============================================================================
        
    
        # Value function
        self.V = np.zeros((self.numStates,1))
        self.Vnew = np.zeros((self.numStates,1))
        
        self.eps = 1e-6
        
        #Policy
        self.policy = np.random.randint(0,3,(1,self.numStates))
        
        #Discount factor
        self.gamma = gamma
        
        self.epsilon = initial_exploration_rate
        self.change = 0
        
        print("Stepper1.loadPolicyAndV() called")
        self.loadPolicyAndV()       
        
        self.episode = 1
        
    def init(self):
        """Reset and setup the stepper"""

        print("Stepper1.init() called")

        # Tell the environment to place the agent at the beginning
        self.env.reset()

        # Ensure the agent's position is properly displayed
        self.dispatch.render()
        self.lastRenderingTime = time.perf_counter()

        self.cardinalPoints = ['E', 'S', 'W', 'N', 'E']
        
    
    def increaseCounters(self,choice,lastState,currentState):

        # Action: Advance
        if (choice == 0):
            row = ((lastState[2]*self.numCells) + (lastState[0])*self.yCells
                   + lastState[1])
            column = ((currentState[2]*self.numCells) + (currentState[0])*self.yCells
                   + currentState[1])
             
            self.numCounterA[row,column] += 1
            self.denCounterA[row] += 1
            
        # Action: Turn Right
        if (choice == 1):
            row = ((lastState[2]*self.numCells) + (lastState[0])*self.yCells
                   + lastState[1])
            column = ((currentState[2]*self.numCells) + (currentState[0])*self.yCells
                   + currentState[1])
            
            self.numCounterR[row,column] += 1
            self.denCounterR[row] += 1

        # Action: Turn Left
        if (choice == 2):
            row = ((lastState[2]*self.numCells) + (lastState[0])*self.yCells
                   + lastState[1])
            column = ((currentState[2]*self.numCells) + (currentState[0])*self.yCells
                   + currentState[1])
            
            self.numCounterL[row,column] += 1
            self.denCounterL[row] += 1
    
    def advance(self,lastState,apx,apy):
        
        # apx and apy are the current position
        
        # Calculating step to reach next cell's center
        if lastState[2] == 0:
            
            # Next cell center 
            nextx = (lastState[0]+2)*self.cellSizeX - self.cellSizeX/2
            nexty = (lastState[1]+1)*self.cellSizeY - self.cellSizeY/2
            
            difx = nextx - apx
            dify = nexty - apy

        elif lastState[2] == 1:
            
            # Next cell center 
            nextx = (lastState[0]+1)*self.cellSizeX - self.cellSizeX/2
            nexty = (lastState[1]+2)*self.cellSizeY - self.cellSizeY/2
                
            difx = nextx - apx
            dify = nexty - apy
                
        elif lastState[2] == 2:
            
            # Next cell center 
            nextx = (lastState[0]  )*self.cellSizeX - self.cellSizeX/2
            nexty = (lastState[1]+1)*self.cellSizeY - self.cellSizeY/2
                
            difx = nextx - apx
            dify = nexty - apy

        elif lastState[2] == 3:
            
            # Next cell center 
            nextx = (lastState[0]+1)*self.cellSizeX - self.cellSizeX/2
            nexty = (lastState[1]  )*self.cellSizeY - self.cellSizeY/2
                
            difx = nextx - apx
            dify = nexty - apy

        step = math.sqrt(difx**2 + dify**2)
        self.env.tryAction(self.env.agent, lambda: self.env.agent.advance(step))

    def rotate(self,lastState,apx,apy,aa,choice):
        
        # apx and apy are the current position
        # aa is the current angle
        
        if choice == 1: # Turn Right

            if lastState[2] == 0: # Last dir: East
                
                # Next cell center (after spinnig):
                nextx = (lastState[0]+1)*self.cellSizeX - self.cellSizeX/2
                nexty = (lastState[1]+2)*self.cellSizeY - self.cellSizeY/2
            
                difx = nextx - apx
                dify = nexty - apy
                
                spin1 = 90-aa
                spin2 = -math.atan(difx/dify)*180/math.pi
                                    
            elif lastState[2] == 1: # Last dir: South
                
                # Next cell center (after spinnig):
                nextx = (lastState[0]  )*self.cellSizeX - self.cellSizeX/2
                nexty = (lastState[1]+1)*self.cellSizeY - self.cellSizeY/2
                
                difx = nextx - apx
                dify = nexty - apy
                    
                spin1 = 180-aa
                spin2 = math.atan(dify/difx)*180/math.pi
                                        
            elif lastState[2] == 2: # Last dir: West
                
                # Next cell center (after spinnig):
                nextx = (lastState[0]+1)*self.cellSizeX - self.cellSizeX/2
                nexty = (lastState[1]  )*self.cellSizeY - self.cellSizeY/2
            
                difx = nextx - apx
                dify = nexty - apy
                    
                spin1 = 270-aa
                spin2 = -math.atan(difx/dify)*180/math.pi

            elif lastState[2] == 3: # Last dir: North
                
                # Next cell center (after spinnig):
                nextx = (lastState[0]+2)*self.cellSizeX - self.cellSizeX/2
                nexty = (lastState[1]+1)*self.cellSizeY - self.cellSizeY/2
                
                difx = nextx - apx
                dify = nexty - apy
                    
                spin1 = 0-aa
                spin2 = math.atan(dify/difx)*180/math.pi
        
        if choice == 2: # Turn Left

            if lastState[2] == 0: # Last dir: East
                
                # Next cell center (after spinnig):
                nextx = (lastState[0]+1)*self.cellSizeX - self.cellSizeX/2
                nexty = (lastState[1]  )*self.cellSizeY - self.cellSizeY/2
                
                difx = nextx - apx
                dify = nexty - apy
                    
                spin1 = 270-aa
                spin2 = -math.atan(difx/dify)*180/math.pi                
                                    
            elif lastState[2] == 1: # Last dir: South
                
                # Next cell center (after spinnig):
                nextx = (lastState[0]+2)*self.cellSizeX - self.cellSizeX/2
                nexty = (lastState[1]+1)*self.cellSizeY - self.cellSizeY/2
                
                difx = nextx - apx
                dify = nexty - apy
                    
                spin1 = 0-aa
                spin2 = math.atan(dify/difx)*180/math.pi
                    
            elif lastState[2] == 2: # Last dir: West
                
                # Next cell center (after spinnig):
                nextx = (lastState[0]+1)*self.cellSizeX - self.cellSizeX/2
                nexty = (lastState[1]+2)*self.cellSizeY - self.cellSizeY/2
                
                difx = nextx - apx
                dify = nexty - apy
                    
                spin1 = 90-aa
                spin2 = -math.atan(difx/dify)*180/math.pi                   

            elif lastState[2] == 3: # Last dir: North
                
                # Next cell center (after spinnig):
                nextx = (lastState[0]  )*self.cellSizeX - self.cellSizeX/2
                nexty = (lastState[1]+1)*self.cellSizeY - self.cellSizeY/2
            
                difx = nextx - apx
                dify = nexty - apy
                    
                spin1 = 180-aa
                spin2 = math.atan(dify/difx)*180/math.pi                


        totalSpin = spin1 + spin2
        self.env.tryAction(self.env.agent, lambda: self.env.agent.right(totalSpin))

# fixedAdvance currently unused
    def fixedAdvance(self,lastState,apx,apy,aa):
        
        if lastState[2] == 0:
                
            difx = (lastState[0]+2)*self.cellSizeX - self.cellSizeX/2 - apx
            dify = (lastState[1]+1)*self.cellSizeY - self.cellSizeY/2 - apy
                
            spin1 = 0-aa
            spin2 = math.atan(dify/difx)*180/math.pi
                
            totalSpin = spin1 + spin2
            step = math.sqrt(difx**2 + dify**2)

            self.env.tryAction(self.env.agent, lambda: self.env.agent.right(totalSpin))
            self.env.tryAction(self.env.agent, lambda: self.env.agent.advance(step))
            self.env.tryAction(self.env.agent, lambda: self.env.agent.left(spin2))
            
        elif lastState[2] == 1:
                
            difx = (lastState[0]+1)*self.cellSizeX - self.cellSizeX/2 - apx
            dify = (lastState[1]+2)*self.cellSizeY - self.cellSizeY/2 - apy
                
            spin1 = 90-aa
            spin2 = -math.atan(difx/dify)*180/math.pi
                
            totalSpin = spin1 + spin2
            step = math.sqrt(difx**2 + dify**2)

            self.env.tryAction(self.env.agent, lambda: self.env.agent.right(totalSpin))
            self.env.tryAction(self.env.agent, lambda: self.env.agent.advance(step))
            self.env.tryAction(self.env.agent, lambda: self.env.agent.left(spin2))
                
        elif lastState[2] == 2:
                
            difx = (lastState[0])*self.cellSizeX - self.cellSizeX/2 - apx
            dify = (lastState[1]+1)*self.cellSizeY - self.cellSizeY/2 - apy
                
            spin1 = 180-aa
            spin2 = -math.atan(dify/difx)*180/math.pi
                
            totalSpin = spin1 + spin2
            step = math.sqrt(difx**2 + dify**2)
                
            self.env.tryAction(self.env.agent, lambda: self.env.agent.right(totalSpin))
            self.env.tryAction(self.env.agent, lambda: self.env.agent.advance(step))
            self.env.tryAction(self.env.agent, lambda: self.env.agent.left(spin2))
                
        elif lastState[2] == 3:
                
            difx = (lastState[0]+1)*self.cellSizeX - self.cellSizeX/2 - apx
            dify = (lastState[1])*self.cellSizeY - self.cellSizeY/2 - apy
                
            spin1 = 270-aa
            spin2 = -math.atan(difx/dify)*180/math.pi
                
            totalSpin = spin1 + spin2
            step = math.sqrt(difx**2 + dify**2)

            self.env.tryAction(self.env.agent, lambda: self.env.agent.right(totalSpin))
            self.env.tryAction(self.env.agent, lambda: self.env.agent.advance(step))
            self.env.tryAction(self.env.agent, lambda: self.env.agent.left(spin2))
    
    # returnToCenter currently unused
    def returnToCenter(self,lastState,currentState,apx,apy):
        
        if lastState == currentState:
            centerX = (currentState[0]+1)*self.cellSizeX - self.cellSizeX/2
            centerY = (currentState[1]+1)*self.cellSizeY - self.cellSizeY/2
            difx = apx - centerX
            dify = apy - centerY
            
            if (currentState[2]==0 or currentState[2]==2):
                
                step = -abs(difx)
                self.env.tryAction(self.env.agent, lambda: self.env.agent.advance(step))
                
                if (abs(dify) > self.cellSizeY/4):
                    self.env.tryAction(self.env.agent, lambda: self.env.agent.left(90))
                    self.env.tryAction(self.env.agent, lambda: self.env.agent.advance(dify))
                    self.env.tryAction(self.env.agent, lambda: self.env.agent.right(90))
                    
            elif (currentState[2]==1 or currentState[2]==3):
            
                step = -abs(dify)
                self.env.tryAction(self.env.agent, lambda: self.env.agent.advance(step))
                
                if (abs(difx) > self.cellSizeX/4):
                    self.env.tryAction(self.env.agent, lambda: self.env.agent.right(90))
                    self.env.tryAction(self.env.agent, lambda: self.env.agent.advance(difx))
                    self.env.tryAction(self.env.agent, lambda: self.env.agent.left(90))
        
    def valueIteration(self):
        
        # Calculating Probabilities
        # 384 x 384
        # 1,1E 1,2E... 12,8E... 
        PsA = self.numCounterA / self.denCounterA # Advance
        PsR = self.numCounterR / self.denCounterR # Right
        PsL = self.numCounterL / self.denCounterL # Left
                
        Ps = np.array([PsA,PsR,PsL])
        
        iter = 1

        while(True):
        
            self.change = 0
            
            #Calculating the value iteration
            for s in range(self.numStates):
    
                #v_sp = np.reshape(self.V,(self.numStates,1))
               
                psa = (np.transpose(Ps))[0:self.numStates,s,:] # reshape as in octave example
                
                rsa = (np.transpose(self.Rewards))[0:self.numStates,s,:]
                
                suma = np.sum(( psa*(rsa + self.gamma*self.V) ), axis=0)
                
                maxVal = np.max(suma)
                bestAction = np.argmax(suma)
                
                newVal = maxVal
                #newVal = self.Rewards[s,0] + self.gamma*maxVal
                
                if (newVal > (self.V[s,0] + self.eps)):
                    self.policy[0,s] = bestAction
                
                self.Vnew[s,0] = newVal
                
                self.change += abs(newVal-self.V[s,0])
                    
            self.V = cp.copy(self.Vnew)
            
            # print("Value iteration #",iter,"  change: ",self.change)
            if (self.change <= 1e-4 or iter == 2):                
                break
            iter +=1
          
            
    def step(self, iteration):
        """Perform one simulation step"""
        
        if self.steps == 0:
            self.start = datetime.now()
            
        # How to extract the agent's position and orientation
        apx = self.env.agent.state.posX
        apy = self.env.agent.state.posY
        aa  = self.env.agent.state.angle
        
        # Which direction the agent is facing?
        # 0 = East, 1 = South, 2 = West, 3 = North
        dir = math.floor((aa+45)/90)
        if (dir == 4): dir = 0
        
        # How to determine which cell the agent is in
        # Current position:
        cx = math.floor(apx/self.env.maze.cellSizeX)
        cy = math.floor(apy/self.env.maze.cellSizeY)

        # State before action
        lastState = [cx,cy,dir]
        
        
        # Taking action
        randomNumber = random.random()
        if (randomNumber <= self.epsilon):
            
        # Random choice from self.actions
        # 0 = advance, 1 = right, 2 = left
            randomChoice = random.random()
            
            if (randomChoice <= 0.5):
                choice = 0
            elif(randomChoice <= 0.75):
                choice = 1
            else:
                choice = 2
        
        else:
            Policy_action = ((lastState[2]*self.numCells) + (lastState[0])*self.yCells
                       + lastState[1])
            
            choice = self.policy[0,Policy_action]
    
        if choice == 0:
            self.advance(lastState, apx, apy)
        else:
            self.rotate(lastState, apx, apy, aa, choice)
    
        # Current state:
        apx = self.env.agent.state.posX
        apy = self.env.agent.state.posY
        aa  = self.env.agent.state.angle
        dir = math.floor((aa+45)/90)
        if (dir == 4): dir = 0
        cx = math.floor(apx/self.env.maze.cellSizeX)
        cy = math.floor(apy/self.env.maze.cellSizeY)
        currentState = [cx,cy,dir]

        # Increase counters:
        self.increaseCounters(choice,lastState,currentState)

        # Value iteration every step
        self.valueIteration()
            
            
        # Only at most 5fps:
        # - Print information about current position 
        # - Draw the agent's position and (if activated) the sensors
        if (time.perf_counter() - self.lastRenderingTime) > 1/5:
            
            print("Stepper1.step(",iteration,") Steps on current Episode: ", \
                  self.steps)
            print("  Episode: ",self.episode,"  Epsilon: ",self.epsilon)
            
            #print("  Agent continous state: ", self.env.agent)
            print("  Agent last state: ({},{})@{}".\
                  format(lastState[0], #cx
                         lastState[1], #cy
                         self.cardinalPoints[lastState[2]], #dir
                         ))
            
            if (randomNumber <= self.epsilon):
                print("  Random Step")
            else:
                print("  Policy Step")
                
            # Printing action made
            if (choice == 0):
                print("  Action: Advance")
            elif (choice == 1):
                print("  Action: Turn Right")
            else:
                print("  Action: Turn Left")
            
            print("  Agent current state: ({},{})@{}".\
                  format(currentState[0], #cx
                         currentState[1], #cy
                         self.cardinalPoints[currentState[2]], #dir
                         ))
            print("")
            self.dispatch.render()
            self.lastRenderingTime = time.perf_counter()
        
        # Increasing steps counter
        self.steps += 1
        
        # Check which is the finishing cell
        finX, finY = self.env.maze.endX, self.env.maze.endY
        
        # Check if the agent has reached the finish cell
        if currentState[0] == finX and currentState[1] == finY:
            
            print("GOAL REACHED!")
            
            # Measuring time
            self.end = datetime.now()
            time_taken = str(self.end - self.start)
            
            # Storing info
            self.info.append([self.episode,round(self.epsilon,2),self.steps,time_taken])
            
            # Restarting episode steps counter
            self.steps = 0
            
            # Reducing exploration rate
            self.epsilon = exploration_decay*self.epsilon
            if self.epsilon < min_exploration_rate:
                self.epsilon = min_exploration_rate
               
            # Save counters, policy and value function
            self.saveAll()
            
            #Choosing how many episodes:
            if self.episode <max_episodes:
                self.init()
                self.episode += 1
            
            # saving info
            else:
                
                avg_steps = 0
                for i in range(len(self.info)):
                    avg_steps += self.info[i][2]
                
                avg_steps = avg_steps / len(self.info)
                
                original_stdout = sys.stdout
                with open('info.txt', 'w') as f:
                    sys.stdout = f # Change the standard output to the file we created.
                    print("\n  Episode\t\tEpsilon\t\t\tSteps\t\t\tTime")
    
                    for i in range(len(self.info)):
                        print("-"*58)
                        print("\t%-4s\t\t  %-.4s\t\t\t %-4.8s\t\t %-.10s"
                              %(self.info[i][0],
                                self.info[i][1],
                                self.info[i][2],
                                self.info[i][3]))
                
                    end = datetime.now()
                    total_time = str(end - self.total_time)
                    
                    print("\nAverage steps: ", avg_steps)
                    print("Total elapsed time: ", total_time)
                    
                    sys.stdout = original_stdout
                
                # Ploting number of steps vs episode
                
                info = np.asarray(self.info)
                
                fig = plt.figure()
                ax1 = fig.add_subplot(1,1,1)
                
                ax1.plot(info[:,0].astype(int), info[:,2].astype(int))
                ax1.set_xlabel("Episode")
                ax1.set_ylabel("Steps")
      
                ax2 = ax1.twiny()
                ax2.set_xticklabels(np.round(np.linspace(info[0,1].astype(float),info[-1,1].astype(float),6),3))
                #ax2.set_xlim(ax1.get_xlim())
                ax2.set_xlabel("Exploration rate")
                
                plt.show()
                
                self.info = []
  
                self.dispatch.pause()
                self.episode = 1
            # self.dispatch.restart()  # We could restart instead!
        
# #######################
# # Main control center #
# #######################

# This object centralizes everything
theDispatcher = dispatcher.Dispatcher()

# Provide a new environment (maze + agent)
theDispatcher.setEnvironment(env.Environment(12, 8, 15))

# Provide also the simulation stepper, which needs access to the
# agent and maze in the dispatcher.
theDispatcher.setStepper(Stepper1(theDispatcher))

# Start the GUI and run it until quit is selected
# (Remember Ctrl+\ forces python to quit, in case it is necessary)
theDispatcher.run()


